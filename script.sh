#!/bin/bash

pastaParaSites=sites_baixados
pastaParaSitesCompactados=sites_compactados

rm -rf $pastaParaSites

mkdir -p $pastaParaSites
mkdir -p $pastaParaSitesCompactados

if [ $1 ]; then
	./programa_hash $1
	if [ -e "saida.zip" ]; then
		./fgk "saida.zip" d "saida.html"
		rm saida.zip
	fi
else
	contador=0
	for url in $(cat historico.txt); do
		contador=$(($contador + 1))
		wget $url -O "$pastaParaSites/$contador.html"
		./fgk "$pastaParaSites/$contador.html" e "$pastaParaSitesCompactados/$contador.zip"
		./programa_hash $url "$pastaParaSitesCompactados/$contador.zip"
	done
fi
