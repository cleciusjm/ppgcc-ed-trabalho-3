#include <stdio.h>

#define TAMANHO 23

struct item
{
    char *link;
    int posicao;
    int tamanho;
    struct item *prox;
};

struct tabela
{
    struct item *items[TAMANHO];
};

struct item *tabela_buscar(struct tabela *tabela, char *link);

void tabela_inserir(struct tabela *tabela, struct item *value);
