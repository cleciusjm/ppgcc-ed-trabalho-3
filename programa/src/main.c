#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tabela.h"

struct tabela tabela;
char *link, *arquivo;
FILE *arquivo_tar;

void ler_tabela()
{
    for (int i = 0; i < TAMANHO; i++)
    {
        tabela.items[i] = NULL;
    }
    FILE *arquivo_tabela = fopen("tabela.txt", "r+");
    int posicao, tamanho, i = -1;
    char *aux = NULL;
    if (arquivo_tabela != NULL)
    {
        while (!feof(arquivo_tabela) && !ferror(arquivo_tabela))
        {
            aux = (char *)malloc(sizeof(char) * 1000);
            if (!fscanf(arquivo_tabela, "%d | %s | %d | %d\n", &i, aux, &posicao, &tamanho))
            {
                break;
            }
            if (i >= 0 && i < TAMANHO)
            {
                struct item *aux_item = (struct item *)malloc(sizeof(struct item));
                printf("Lendo: %d | %s | %d | %d\n", i, aux, posicao, tamanho);
                aux_item->link = aux;
                aux_item->posicao = posicao;
                aux_item->tamanho = tamanho;
                struct item *item = tabela.items[i];
                if (item == NULL)
                    tabela.items[i] = aux_item;
                else
                {
                    while (1)
                    {
                        if (item->prox != NULL)
                            item = item->prox;
                        else
                            break;
                    }
                    item->prox = aux_item;
                }
            }
        }
        fclose(arquivo_tabela);
    }
}
void salvar_tabela()
{
    FILE *arquivo_tabela = fopen("tabela.txt", "w");
    struct item *item;

    for (int i = 0; i < TAMANHO; i++)
    {
        item = tabela.items[i];
        if (item != NULL)
        {
            printf("Gravando: %d | %s | %d | %d\n", i, item->link, item->posicao, item->tamanho);
            fprintf(arquivo_tabela, "%d | %s | %d | %d\n", i, item->link, item->posicao, item->tamanho);
            if (item->prox != NULL)
            {
                while (1)
                {
                    item = item->prox;
                    if (item == NULL)
                        break;
                    printf("Gravando: %d | %s | %d | %d\n", i, item->link, item->posicao, item->tamanho);
                    fprintf(arquivo_tabela, "%d | %s | %d | %d\n", i, item->link, item->posicao, item->tamanho);
                }
            }
        }
    }

    fclose(arquivo_tabela);
}

struct item *escrever_tar()
{
    int posicao, tamanho;
    FILE *arq_aux;
    fseek(arquivo_tar, 0, SEEK_END); // vai pro fim do arquivo tar
    posicao = ftell(arquivo_tar);
    arq_aux = fopen(arquivo, "r+");
    if (arq_aux != NULL)
    {

        fseek(arq_aux, 0, SEEK_END); // vai pro final

        tamanho = ftell(arq_aux); //pega a posicao final

        fseek(arq_aux, 0, SEEK_SET); //volta pro começo

        char conteudo[tamanho];
        fread(conteudo, 1, tamanho, arq_aux);
        fwrite(conteudo, 1, tamanho, arquivo_tar);

        fclose(arq_aux);
        struct item *item = (struct item *)malloc(sizeof(struct item));
        item->tamanho = tamanho;
        item->posicao = posicao;
        item->link = link;
        return item;
    }
    return NULL;
}
void ler_tar(struct item *item)
{
    FILE *arquivo_saida = fopen("saida.zip", "w+");
    int tamanho = item->tamanho;
    int posicao = item->posicao;

    fseek(arquivo_tar, posicao, SEEK_SET);

    char conteudo[tamanho];
    fread(conteudo, 1, tamanho, arquivo_tar);
    fwrite(conteudo, 1, tamanho, arquivo_saida);
}

int main(int argc, char **argv)
{
    arquivo_tar = fopen("paginas.tar", "a+");
    printf("iniciando main\n");
    ler_tabela();
    if (argc == 2) // buscando
    {

        link = argv[1];
        printf("buscar %s\n", link);
        struct item *item = tabela_buscar(&tabela, link);
        if (item == NULL)
            printf("não achou na tabela\n");
        else
        {
            printf("achou na posição %d\n", item->posicao);
            ler_tar(item);
        }
    }
    else if (argc == 3) //inserindo
    {

        link = argv[1];
        arquivo = argv[2];
        printf("inserir %s %s\n", link, arquivo);

        struct item *item = escrever_tar();
        tabela_inserir(&tabela, item);
    }
    salvar_tabela();
    fclose(arquivo_tar);
    return 0;
}
