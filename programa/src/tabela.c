#include "tabela.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int codigo_hash(char *link)
{
    int tamanho = strlen(link);
    int valor_int = 0;
    for (int i = 0; i < tamanho; i++)
    {
        valor_int = link[i] + valor_int;
    }
    return valor_int % TAMANHO;
}

struct item *tabela_buscar(struct tabela *tabela, char *link)
{
    int cod = codigo_hash(link);

    struct item *item = tabela->items[cod];

    while (item != NULL)
    {
        int comparacao = strcmp(item->link, link);
        if (comparacao == 0)
            return item;
        else
            item = item->prox;
    }

    return NULL;
}

void tabela_inserir(struct tabela *tabela, struct item *item)
{
    char *link = item->link;
    int cod = codigo_hash(link);

    struct item *aux = tabela->items[cod];
    int encontrou = aux == NULL;

    if (encontrou)
    {
        tabela->items[cod] = item;
    }
    else
    {
        int comparacao = strcmp(aux->link, link);
        if (comparacao != 0)
        {
            while (1)
            {
                if (aux->prox != NULL)
                    aux = aux->prox;
                else
                    break;
            }
            aux->prox = item;
        }
    }
}